ANSIBLE_CACHE_PLUGIN = yaml
ANSIBLE_CACHE_PLUGIN_CONNECTION = ./facts
ANSIBLE_CONFIG = /etc/ansible/ansible.cfg

# Gives Ansible access to SSH Agent
export

tasks := $(patsubst ./%.yml,%,$(wildcard ./*.yml))
$(tasks): always
	/usr/bin/time ansible-playbook --forks 50 --ask-vault-pass $@.yml


.PHONY: always
always:
