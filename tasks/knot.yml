---
# This is the DNS configuration.  We're using knot because it has a
# lower memory footprint.
#
# Typical nameserver deploys for a domain are hierarchical, a single
# node acts as origin of DNS information and the next servers replicate.
#
# DNS resolvers will check randomly for nameservers, but when you
# publish several IP addresses (A* records), the resolver will pick one
# at random, but the system won't retry if that one doesn't respond,
# because the resolver only informed a single non-functional address.
# This means you have to update and replicate your DNS records whenever
# a server is down.
#
# In our case, we want each node to serve its own IP addresses.  When a
# node is down (for maintenance, accident or for good), it takes those
# IP addresses with them.  This means a node can be down and we won't
# have to update this information on the zonefile.  Resolvers that try
# to reach a nameserver that's down will retry with the next one,
# that'll have the working addresses.

- name: remove domains
  file:
    state: absent
    path: "{{ root }}{{ directories.knot }}/{{ item }}.zone"
  loop: "{{ dns.removed_domains }}"

# Create a temporary directory for each domain served.
- name: tmp dir
  file:
    state: directory
    path: "./dns/{{ item.name }}"
  loop: "{{ dns.domains }}"

# First part for each domain are Sutty's records plus their own specific
# records from config.yml.  Each node has its own information
- name: zone file
  template:
    src: "templates/domain.zone.j2"
    dest: "./dns/{{ item.name }}/00_name.zone"
  loop: "{{ dns.domains }}"

# Upload DKIM records.
- name: dkim records
  when: "item.email is not defined or item.email != false"
  copy:
    src: "./dkim/{{ delegate.hostname }}/srv/sutty/etc/opendkim/dkim.txt"
    dest: "./dns/{{ item.name }}/03_dkim.zone"
  loop: "{{ dns.domains }}"

# Assemble the three parts into a zonefile for each domain.
- name: zone file
  assemble:
    src: "./dns/{{ item.name }}"
    dest: "{{ root }}{{ directories.knot }}/{{ item.name }}.zone"
  loop: "{{ dns.domains }}"

# Create configuration directory
- name: knot dir
  file:
    state: directory
    dest: "{{ root }}{{ directories.etc }}/knot/"

# Create configuration file
- name: knot conf
  template:
    src: "templates/knot.conf.j2"
    dest: "{{ root }}{{ directories.etc }}/knot/knot.conf"
    mode: "644"

- name: manual zones
  copy:
    src: "./manual_zones/{{ item }}.zone"
    dest: "{{ root }}{{ directories.knot }}/{{ item }}.zone"
  loop: "{{ dns.domains }}"
  loop: "{{ dns.manual_zones }}"

# Open ports on the firewall
- name: iptables
  iptables:
    action: append
    chain: INPUT
    table: filter
    destination_port: 53
    protocol: "{{ item }}"
    jump: ACCEPT
  loop:
  - tcp
  - udp

# Set permissions.
- name: knot permissions
  command: "chown -R root:101 {{ root }}{{ directories.etc }}/knot/"

# Set permissions.
- name: knot permissions
  command: "chown -R 100:101 {{ root }}{{ directories.knot }}"

# Set permissions.
- name: knot permissions
  command: "chmod -R u=rwX,g=rX,o= {{ root }}{{ directories.etc }}/knot/ {{ root }}{{ directories.knot }}"

# Start the container.
#
# XXX: We're binding to the default interface because systemd-resolved
# binds to 127.0.0.53:53 and prevents docker to bind to 0.0.0.0:53
- name: knot container
  docker_container:
    name: knot
    image: "{{ registry }}/sutty/knot:3.15.0"
    restart_policy: always
    detach: yes
    state: started
    hostname: "knot.{{ inventory_hostname }}"
    dns_search_domains: "{{ inventory_hostname }}"
    volumes:
    - "/dev/log:/dev/log"
    - "{{ root }}{{ directories.etc }}/knot/:/etc/knot/"
    - "{{ root }}{{ directories.knot }}:{{ directories.knot }}"
    published_ports:
    - "{{ ansible_facts.default_ipv4.address }}:53:53"
    - "{{ ansible_facts.default_ipv4.address }}:53:53/udp"
    env:
      EMAIL: "{{ monit.email }}"
      EMAIL_FROM: "{{ monit.email_from }}"
      DOMAIN: "{{ inventory_hostname }}"
      MMONIT: "mmmonit.{{ delegate.hostname }}:3000"
      CREDENTIALS: "{{ lookup('community.general.random_string', length=12, special=false) }}:{{ lookup('community.general.random_string', length=12, special=false) }}"
    networks:
    - name: sutty
    purge_networks: yes
    tmpfs:
    - /tmp
    - /run

# TODO: Only reload the configuration if the container was running
# already.
- name: knot reload
  command: "docker exec knot knotc reload"
