---
# The Sutty platform for testing.
#
# We're using Jemalloc to reduce RAM usage.
- name: nginx staging
  template:
    src: "templates/panel.staging.conf.j2"
    dest: "{{ root }}{{ directories.nginx }}/panel.staging.{{ sutty }}.conf"

- name: nginx reload
  command: "docker exec nginx nginx -s reload"

# Copy the database from production to staging
- name: staging remove database
  when: "inventory_hostname == delegate.hostname"
  shell: "docker exec postgresql sh -c 'echo drop database if exists staging | psql -U postgres'"

- name: staging create database
  when: "inventory_hostname == delegate.hostname"
  shell: "docker exec postgresql sh -c 'echo create database staging | psql -U postgres'"

- name: staging copy database
  when: "inventory_hostname == delegate.hostname"
  shell: "docker exec postgresql sh -c 'pg_dump -U postgres -d {{ postgresql.database }} --exclude-table-data=access_logs --exclude-table-data=csp_reports --exclude-table-data=build_stats | psql -U postgres -f - -1 staging'"

- name: postgresql trust
  when: "inventory_hostname == delegate.hostname"
  postgresql_pg_hba:
    dest: "{{ root }}{{ directories.postgresql }}/{{ postgresql.version }}/data/pg_hba.conf"
    contype: host
    users: "sutty"
    source: "staging.sutty"
    databases: "staging"
    method: trust

- name: postgresql reload
  when: "inventory_hostname == delegate.hostname"
  shell: "docker exec postgresql /usr/local/bin/postgresql reload"

# @see {tasks/sutty.yml}
- name: sutty staging container
  when: "inventory_hostname == delegate.hostname"
  docker_container:
    debug: yes
    name: staging
    image: sutty/staging
    restart_policy: always
    detach: yes
    state: started
    hostname: "staging.{{ inventory_hostname }}"
    dns_search_domains: "{{ inventory_hostname }}"
    volumes:
    - '/dev/log:/dev/log'
    - "{{ root }}{{ directories.etc }}/credentials.yml.enc"
    - "{{ root }}{{ directories.data }}:{{ directories.data }}"
    - "{{ root }}{{ directories.staging }}:/srv/http/public"
    - "{{ root }}/ssh/sutty:/srv/http/.ssh/"
    env:
      LANG: "C.UTF-8"
      LD_PRELOAD: /usr/lib/libjemalloc.so
      EMAIL: "{{ email }}"
      SUTTY: "{{ sutty }}"
      DATABASE: "staging"
      DOMAIN: "{{ inventory_hostname }}"
      KNOT: "{{ knot_address.stdout }}"
      DELEGATE: "{{ delegate.hostname }}"
      TIENDA: "{{ tienda }}"
      HTTP_BASIC_USER: "{{ vault.api.username }}"
      HTTP_BASIC_PASSWORD: "{{ vault.api.password }}"
      DEFAULT_FROM: "Sutty <noreply@{{ sutty }}>"
      SKEL_SUTTY: "{{ directories.data }}/skel"
      RAILS_MAX_THREADS: "5"
      WEB_CONCURRENCY: "1"
      RAILS_MASTER_KEY: "{{ vault.rails.master_key }}"
      #RAILS_SERVE_STATIC_FILES: "false"
      #RAILS_LOG_TO_STDOUT: "true"
      RAILS_ENV: "production"
      EXCEPTION_TO: "{{ exceptions }}"
      REDIS_SERVER: "redis://redis:6379/10"
      REDIS_CLIENT: "redis://redis:6379/10"
      PORT: "3000"
      LOUNGE: "{{ vault.lounge }}"
      BLAZER_DATABASE_URL: "postgres://blazer:{{ vault.blazer.password }}@postgresql:5432/staging"
      BLAZER_USERNAME: "{{ vault.api.username }}"
      BLAZER_PASSWORD: "{{ vault.api.password }}"
      #BLAZER_SLACK_WEBHOOK_URL: ""
      DEBUG_FORMS: "true"
      COOKIE_DURATION: "120"
      SITE_PATH: "{{ directories.sites }}"
      PANEL_URL: "{{ airbrake.site_url }}"
      AIRBRAKE_SITE_ID: "{{ airbrake.site_id }}"
      AIRBRAKE_API_KEY: "{{ airbrake.api_key }}"
      PIDFILE: "/srv/http/tmp/puma.pid"
      GITLAB_PROJECT: "{{ vault.gitlab.project }}"
      GITLAB_TOKEN: "{{ vault.gitlab.token }}"
      GITLAB_URI: "{{ vault.gitlab.uri }}"
    networks:
    - name: sutty
    purge_networks: yes
