# Deploy Sutty from scratch

This Ansible playbook will connect to [your
servers](https://0xacab.org/sutty/ansible-encrypted-host) and deploy
all of Sutty's required infrastructure.

## Deploy

Add your server(s) to Ansible hosts under the `sutty` group:

```ini
sutty.nl

[sutty]
sutty.nl
```

Copy the `credentials.yml.enc` file created by
[Sutty](https://0xacab.org/sutty/sutty) into this directory.

```bash
# For instance
cp ../sutty/config/credentials.yml.enc .
```

Create an Ansible vault called `vault.yml` and fill it with the contents
of `vault.yml.example` and your passwords (use long and random ones!).

Run the deploy:

```bash
make
# or
ansible-playbook --ask-vault-pass sutty-deploy.yml
```
